/*
Navicat MySQL Data Transfer

Source Server         : 测试环境数据库(财务)202
Source Server Version : 50610
Source Host           : 192.168.1.202:3306
Source Database       : tencentdata

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2017-01-23 14:34:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for share_lists
-- ----------------------------
DROP TABLE IF EXISTS `share_lists`;
CREATE TABLE `share_lists` (
  `id` int(20) NOT NULL COMMENT '股票ID',
  `name` varchar(100) NOT NULL COMMENT '股票名称',
  `count_date` date NOT NULL COMMENT '统计日期',
  `open_price` float(10,2) NOT NULL COMMENT '开盘价',
  `end_price` float(10,2) NOT NULL COMMENT '收盘价',
  `top_price` float(10,2) NOT NULL COMMENT '最高价',
  `low_price` float(10,2) NOT NULL COMMENT '最低价',
  `volume` int(10) NOT NULL COMMENT '成交量（万手）',
  `turnover` float(10,2) NOT NULL COMMENT '换手率',
  `marketvalue` float(10,2) NOT NULL COMMENT '市值（亿）',
  `ratio` float(10,2) NOT NULL COMMENT '市盈率',
  `share_type` int(2) NOT NULL COMMENT '股票类型 （1.上证2.深证）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_time` datetime NOT NULL COMMENT '修该时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_count_date` (`id`,`count_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='股票信息表';
