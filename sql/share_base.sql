/*
 Navicat Premium Data Transfer

 Source Server         : ubuntu
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : 192.168.16.240
 Source Database       : tencentdata

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : utf-8

 Date: 01/24/2017 02:30:31 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `share_base`
-- ----------------------------
DROP TABLE IF EXISTS `share_base`;
CREATE TABLE `share_base` (
  `share_id` int(10) NOT NULL COMMENT '股票ID',
  `create_time` datetime NOT NULL,
  `modify_time` datetime NOT NULL,
  `status` int(2) NOT NULL COMMENT '1.启用，2.停用',
  PRIMARY KEY (`share_id`),
  UNIQUE KEY `share_id` (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='股票基础表';

SET FOREIGN_KEY_CHECKS = 1;
