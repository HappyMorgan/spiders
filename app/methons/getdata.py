#-*-coding:utf-8 -*-
from  urllib import request
from flask import current_app
import urllib

# 获取response的方法
def getHtml(url,timeout = 20,values = None):

    current_app.logger.debug("url is :%s"%url)

    # 假装浏览器
    user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
    headers = { 'User-Agent' : user_agent}

    if values != None:
        data = urllib.parse.urlencode(values)
    else:
        data = None
    request_ = request.Request(url= url,data= data,headers = headers)
    response = request.urlopen(request_,timeout=timeout).read()
    # return response.decode("gbk","ignore")
    return response