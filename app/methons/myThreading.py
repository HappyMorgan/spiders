#-*-coding:utf-8 -*-
import threading
from flask import current_app

class myThread(threading.Thread):

    def __init__(self,func_name,class_path,args):
        super(myThread,self).__init__()
        self.func_name = func_name
        current_app.logger.debug("func_name is :%s" % self.func_name)
        self.path_str = "from " + class_path  + " import " + func_name
        current_app.logger.debug("path_str is :%s"%self.path_str)
        self.args = args
        current_app.logger.debug("args is :%s" % self.args)
        self.lock = threading.Lock()
# todo
    def run(self):
        # exec(self.path_str)
        from app.services.tencentdata import get
        # self.lock.acquire()
        exec(self.func_name)(self.args)
        # self.lock.release()


