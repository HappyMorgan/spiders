#-*-coding:utf-8 -*-
'''
读取配置文件配置，并返回dict
'''
import configparser
from flask import current_app

def getsetting(file_conf,part_name):
    result = {}
    try:
        settings = configparser.ConfigParser()
        file = "../resources/"+file_conf
        settings.read(file)
        set_list = settings.items(part_name)
        for v in set_list:
            result[v[0]] = v[1]
    except Exception as e:
        current_app.logger.error("getsetting error :%s "%e)
    current_app.logger.debug("return result dict:%s"%result)
    return result