#-*-coding:utf-8 -*-
'''
def match_result 传入绝对路径xpath_url，返回标签内容
'''
from lxml import etree
from flask import current_app

def match_result(html,xpath_url):
    # current_app.logger.debug("html is :%s"%html)
    selector = etree.HTML(html)
    content = selector.xpath(xpath_url)
    if len(content) > 1:
        current_app.logger.error("getresult is more than one ---")
    else:
        current_app.logger.debug("xpath result is :%s" % content[0].text)
        return content[0].text
