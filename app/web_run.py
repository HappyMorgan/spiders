#-*-coding:utf-8 -*-
from flask import Flask
from app.methons.myThreading import myThread

app = Flask(__name__)

@app.route("/")
def index():
    for i in range(2):
        v = str(600000 + i)
        if i%2 == 0:
            app.logger.debug("t_1 v is :%s"%v)
            t_1 = myThread("get","app.services.tencentdata",v)
        else:
            app.logger.debug("t_v v is :%s"%v)
            t_2 = myThread("get","app.services.tencentdata",v)
    t_1.start()
    t_1.join()
    # t_2.start()
    # t_2.join()
    # from app.services.tencentdata import get
    # get("600000")
    return "hello"

if __name__ == '__main__':
    app.debug = True
    app.run()