#-*-coding:utf-8 -*-
from app.models.usedb import db

class Sharelists(db.Model):
    __tablename__ = "share_lists"

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.VARCHAR(100))
    count_date = db.Column(db.Date)
    open_price = db.Column(db.Float(10,2))
    end_price = db.Column(db.Float(10,2))
    top_price = db.Column(db.Float(10,2))
    low_price = db.Column(db.Float(10,2))
    volume = db.Column(db.Integer)
    turnover = db.Column(db.Float(10,2))
    marketvalue = db.Column(db.Float(10,2))
    ratio = db.Column(db.Float(10,2))
    share_type = db.Column(db.Integer)
    create_time = db.Column(db.DateTime)
    modify_time = db.Column(db.DateTime)

