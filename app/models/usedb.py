#-*-coding:utf-8 -*-
'''
flask - sqlalchemy 连接mysql bean
'''
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from app.methons import getset

app = Flask(__name__)

db_config = "mysql+pymysql://root:123456@192.168.16.240/tencentdata?charset=utf8"

# try:
#     settings = getset.getsetting("bean.conf", "mysql")
#     db_config = "mysql+pymysql://" + settings["db_user"] + ":" + settings["db_passwd"] + "@" + settings["db_host"] + ":" + settings["db_port"] + "/" + settings["db_name"] + "?charset=utf8"
#     app.logger.debug("db_config is :%s"%db_config)
# except Exception as e:
#     app.logger.error("get dbconfig error : %s" % e)

app.config['SQLALCHEMY_DATABASE_URI'] = db_config
# 打印所的sql
# app.config["SQLALCHEMY_ECHO"] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] =True
db = SQLAlchemy(app)