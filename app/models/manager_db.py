#-*-coding:utf-8 -*-
from app.models.usedb import db

class manager:
    def query(self,farg,type = None,**kwargs):
        if type == None:
            return farg.query.all()
        elif type == 1:
            '''type 1,传入查询dict参数,key为filter'''
            return farg.query.filter(kwargs["filter"]).all()
        elif type == 2:
            '''type 2,传入查询dict参数,key为filter_by'''
            return farg.query.filter_by(kwargs["filter_by"]).first()
        elif type ==3:
            '''type 3,传入查询dict参数,key为filter_by'''
            return farg.query.filter_by(kwargs["filter_by"]).all()
        elif type ==4:
            '''type 4,传入查询dict参数,key为get'''
            return farg.query.get(kwargs["get"])

    def insert_update(self,*args):
        for i in args:
            db.session.add(i)

    def delete(self):
        pass

    def commit(self):
        db.session.commit()

    def rockback(self):
        db.session.rollback()