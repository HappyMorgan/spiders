#-*-coding:utf-8 -*-
import datetime
import re

from  flask import current_app

from app.methons.getdata import getHtml
from app.models.model_object import Sharelists
from app.models.manager_db import manager

# 获取接口数据并插入数据库
def get(share_code,share_type = "sh"):
    current_app.logger.info("start get ...")
    url = "http://web.sqt.gtimg.cn/q="+share_type+share_code+"?r=0.14373250893457712"
    result = getHtml(url).decode("cp936", "ignore")
    current_app.logger.debug("result is :%s" % result)
    deal_getresult(result,share_type)

# 处理数据
def deal_getresult(result,type):
    r = re.split("~",result)
    sharelists = Sharelists()
    sharelists.id = r[2]
    sharelists.name = r[1].encode()
    current_app.logger.debug("insert object: %s" % sharelists.name)
    sharelists.count_date = datetime.date.today()
    sharelists.open_price = r[4]
    sharelists.end_price = r[3]
    sharelists.top_price = r[-17]
    sharelists.low_price = r[-16]
    sharelists.volume = r[-6]
    sharelists.turnover = r[38]
    sharelists.marketvalue = r[-6]
    sharelists.ratio = r[-11]
    if type =="sh":
        sharelists.share_type = 1
    else:
        sharelists.share_type = 2
    sharelists.create_time = datetime.datetime.now()
    sharelists.modify_time = datetime.datetime.now()
    current_app.logger.debug("insert object: %s"%sharelists)
    m = manager()
    # 插入
    try:
        m.insert_update(sharelists)
    except Exception as e:
        current_app.logger.error("insert_update error: %s"%e)
        m.rockback()
    # 提交
    try:
        m.commit()
    except Exception as e:
        current_app.logger.error("commit error: %s" % e)


